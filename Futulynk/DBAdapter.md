# DBAdapter (DataBase Adapter)

### Description and purpose

This project is an implementation of interfaces and abstract classes of the **badpt** (Base Adapter) project. It allows to constantly retrieve values from a DB and pass them to the Kafka broker.
Adapters, in general, are an important part of the Futulynk system.
* ##### What is an adapter?
  An adapter is a set of Java classes which scope is to connect one or more **source devices** to the Kafka broker.
* ##### What is a source device?
  A source device is anything that can return a value from a real-world measurement.  Such as (PLCs, sensors, DBs)

* ##### What does this implementation?
  The DBAdapter implementation makes it possible to use one or more databases as source devices. With that, it's also possible to continuously retrieve values from a DB like a sensor would do.

### Features

* ##### Read values from DB 
  The main purpose of this implementation is to read the results of one or more queries on a database on every refresh.  The results are then sent to a Kafka Broker topic.

* ##### Querying the DB on refresh time
  To constantly obtain data, the queries are executed once per every refresh.  Via configuration a query can be executed with a _BETWEEN_ clause that includes only values with the timestamp in between the current timestamp and the last refresh timestamp.

* ##### Hikari CP connection pooling usage
  Connections to the DBMS are handled with a pool of datasources by Hikari CP, that let's handle connections more efficiently. 
  Hikari CP is a lightweight third part library that in addition to handling connections, also speeds up the execution of queries.

### Settings

* ##### HJSON configuration file 

  To run correctly the DBAdpter, you must set up a configuration file written in HJSON and then pass it as arguments from the command line.
In the configuration file can be specified one or more source device and its variables to retrieve. 

Example of a configuration file that aims to a DB that stores values from a real running engine:
## DBAdpt.conf

```
DBAdpt {
    sourceDevices: [
        {
            streamCode: DBProva
            DBUrl: "jdbc:mariadb://127.0.0.1:3307/assets"
            username: "Admin"
            password: "pass1234"
            minPoolSize: 20
            maxPoolSize: 2500
            variables:[
                {    # AI: rpm - unità di misura: rpm - range [0-3500]
                    streamCode: rpm_1
                    singleResultQuery: "SELECT AVG(rpm) as VALUE FROM enginesdata Where engineID = 1" 
                    resultType: INT
                    enableTimestamp: false
#                   timestampColumnName: timestamp
                    refreshPeriodMs: 1500
                }
                {    # AI: vibrations - unità di misura: mm/s - range: [0-50]
                    streamCode: vibrations_1
                    singleResultQuery: "SELECT AVG(vibrations) as VALUE FROM enginesdata Where engineID = 1" 
                    resultType: SHORT
                    enableTimestamp: false
#                   timestampColumnName: timestamp
                    refreshPeriodMs: 3500
                }
            ]
        }
    ]
}
```
###### Note:
_Is allowed to specify more than one source device at the same time._
_Is also allowed to specify more than one variable for every source device at the same time._

* ##### Source device
  The parameters of sourceDevice are:
  * **streamCode** - (String) a unique ID to identify the device **required**
  * **DBUrl** - (String) JDBC URL for the connection to the Database (**required**)
  * **username** - (String) username to access the DB (_**optional** if passed with DBUrl_)
  * **password** - (String) password to access the DB (_**optional** if passed with DBUrl_)
  * **minPoolSize** - (Number) minum idle connections in the pool (**required**)
  * **maxPoolSize** - (Number) max connections in the pool (**required**)
  * **variables**  - (Object array) array of variables to retrieve (**required**)

* ##### Variables 
  The parameters of variables are:
  * **streamCode** - (String) a unique ID to identify the variable (**required**)
  * **singleResultQuery** - (String) query to execute on the DB (**required**)
  * **resultType** - (Number)  data type of the query result (**required**)
  * **enableTimestamp** - (Boolean) determinates if the between clause should be used or not. (**required**)
  * **timestampColumnName** - (String) Name of the column that contains timestamps in the table of the DB. (_**optional** if enableTimestamp is false_)
  * **refreshPeriodMs** - (Number) Milliseconds to wait before re-executed the query on the DB (**required**)

* ### Run from the console
  To run the DBAdapter from the console, you need to pass some required arguments.

    ```
    --kafka "bootstrap.servers=kafka.futuryng.com:9093"
    --sslConfig conf/client-ssl.properties
    dbadpt
    --conf conf/DBAdpt.conf
    --dataTopic ingestData.pbuf
    ```

    **--kafka** : url and port of kafka broker bootstrap server
    **--sslConfing** : path of the client-ssl properties file
    **dbadpt** : command to execute de adapter 
    **--conf** : path of the configuration file 
    **--dataTopic**: path of ingestData.pbuf (kafka topic)





 
