# Grafana plugins development

There are different ways to approach Grafana plugins development, I personally prefer this.

### Files structure

```
  /dist
  /node_modules
  /src
    - /css
      -<pluginName>.css
    - /external
      - /ext_library.js
      - /...
    - /img
    - <pluginName>_ctrl.js
    - editor.html
    - module.html
    - module.js
  Gruntfile.js
  LICENSE
  package.json
  plugin.json
  README.md
  ```

To use this method you need these programs installed:

* [Docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
* [nodejs](https://nodejs.org/it/download/current/) 
* [npm](https://www.npmjs.com/) 
* [grunt-cli](https://gruntjs.com/) (`npm install -g grunt-cli`)

Install all of the above software, and continue.

### To set up the plugin development follow these steps:

* #### Create a development directory and `cd` to it.

* #### Execute `npm init`.
  
  A process will help you through the creation of the **package.json** file.

* #### Create another file called **plugin.json** 

  The file should look like that:

  ```
  {
      "type": "(panel, app or dataSource)",
      "name": "pluginName",
      "id": "grafana-pluginName-type",

      "info": {
          "description": "name of the plugin",
          "author": {
            "name": "Author name"
            
          },
          "keywords": ["pluginName", "type"],
          "version": "1.0.0",
          "updated": "2018-09-06"
        },

        "dependencies": {
          "grafanaVersion": "3.x.x",
          "plugins": [ ]
        }
  }
  ```

  This file is necessary for Grafana to recognize the final build as a plugin.

* #### Now let's install some useful dependencies:

  Install them with the command:
  ```npm i -D babel babel-plugin-transform-es2015-modules-systemjs babel-preset-es2015 grunt-babel grunt-contrib-clean grunt-contrib-copy grunt-contrib-uglify grunt-contrib-watch grunt-execute grunt-systemjs-builder load-grunt-tasks```

  * [babel](https://babeljs.io/)
  * babel-plugin-transform-es2015-modules-systemjs
  * babel-preset-es2015
  * grunt-babel
  * grunt-contrib-clean
  * grunt-contrib-copy
  * grunt-contrib-uglify
  * grunt-contrib-watch
  * grunt-execute
  * grunt-systemjs-builder
  * load-grunt-tasks

* #### Now it's time to install [Grunt](https://gruntjs.com/) locally in the directory.

  To do that:

  `cd` to the working directory and then
  
  `npm install grunt --save-dev`

  Grunt is a helpful tool that allows to build the plugin automatically. It puts the build in a folder called **dist**.

  Then create a Gruntfile.js in the main directory of the plugin:
  ```
  module.exports = (grunt) => {
  require('load-grunt-tasks')(grunt);

  grunt.loadNpmTasks('grunt-execute');
  grunt.loadNpmTasks('grunt-contrib-clean');

  grunt.initConfig({

    clean: ['dist/**/*'],

    copy: {
      src_to_dist: {
        cwd: 'src',
        expand: true,
        src: ['**/*', '!**/*.js', '!**/*.scss', '!img/**/*'],
        dest: 'dist'
      },
      pluginDef: {
        expand: true,
        src: ['plugin.json', 'README.md'],
        dest: 'dist',
      },
      img_to_dist: {
        cwd: 'src',
        expand: true,
        src: ['img/**/*'],
        dest: 'dist/src/'
      },
      externals: {
        cwd: 'src',
        expand: true,
        src: ['**/external/*'],
        dest: 'dist'
      }
    },

    watch: {
      rebuild_all: {
        files: ['src/**/*', 'plugin.json'],
        tasks: ['default'],
        options: {spawn: false}
      },
    },

    babel: {
      options: {
        sourceMap: true,
        presets: ['es2015'],
        plugins: ['transform-es2015-modules-systemjs', 'transform-es2015-for-of'],
      },
      dist: {
        files: [{
          cwd: 'src',
          expand: true,
          src: ['*.js'],
          dest: 'dist',
          ext: '.js'
        }]
      },
    },

  });

  grunt.registerTask('default', ['clean', 'copy:src_to_dist', 'copy:pluginDef', 'copy:img_to_dist', 'copy:externals', 'babel']);
  };

  ```

  You can use grunt with those commands: 
  
  `grunt` or `grunt watch`

  First build the plugin once. Then grunt will automatically build project every time it notices that in the working directory a file is changed. We will use it later.


* #### Run the container with Grafana

  ```
  docker run -p 3000:3000 -d --name grafana-plugin-dev \
  --volume $(pwd)/dist:/var/lib/grafana/plugins/pluginName \
  grafana/grafana
  ```

  This will launch a grafana-server with the **build folder** (dist) mounted in the file system of the container. When the container is up and running, go to localhost:3000 to access the Grafana instance and try out your plugin.

* #### Start develop

  In a terminal run `grunt watch` and maintain the process open.

  Now for every change, you make it's not necessary to restart the container, the plugin will change in real time with no errors. The only time you need to restart the docker container is when 'plugin.json' is modified.

  NOTE: This is not true for the Development process of a datasource plugin. In that case, you need to restart for every single change you make.
  
  `docker restart grafana-plugin-dev`

* #### The structure of a panel plugin

  A panel plugin project is structured like that:

  ```
  /dist
  /node_modules
  /src
    - /css
      -<pluginName>.css
    - /external
      - /ext_library.js
      - /...
    - /img
    - <pluginName>_ctrl.js
    - editor.html
    - module.html
    - module.js
  Gruntfile.js
  LICENSE
  package.json
  plugin.json
  README.md
  ```

  * The dist (build) folder is automatically created at the first grunt compilation.

  * The external folder is only needed if you have to insert in the project external js libraries. Note that not all external libraries are supported by Grafana.

  * The file **pluginName_ctrl.js** is the main js file, is needed and must extend one of this two grafana classes:
  
    * PanelCtrl
    * MetricsPanelCtrl

    The main and only difference between them is that MetricsPanelCtrl allows the plugin to have the Metrics tab in editor mode and accordingly the possibility to select a datasource and take the data stream from Grafana.

    An example of **pluginName_ctrl.js**

    ```
    import {
      MetricsPanelCtrl
    } from 'app/plugins/sdk';
    import _ from 'lodash';
    import AutomatonGraphDrawer from './automaton_graph_cyto';
    
    const panelDefaults = {
      label: '',
      transitionsColor: '#000'
    }

    export class AutomatonVisualizer_Ctrl extends MetricsPanelCtrl {

      constructor($scope, $injector) {
        super($scope, $injector);

        _.defaults(this.panel, panelDefaults);

        this.events.on("data-received", this.onDataReceived.bind(this));
        this.events.on('panel-initialized', this.render.bind(this));
        this.events.on('init-edit-mode', this.onInitEditMode.bind(this));
        
      }

      onInitEditMode() {
        this.addEditorTab('Settings', 'public/plugins/trovato-automatonvisualizer-panel/editor.html', 2);
      }

      /*overridden method of metricsPanelCtrl,
      * allow to reference module.html tags in this js
      * allow to access other data of the grafana istance*/
      link(scope, elem, attrs, ctrl) {
        //console.log(ctrl.panel.datasource) 
        cyContainer = elem.find('#cy');
      }

      //method called on event data-received
      onDataReceived(data) {
        if (!automatonDefinitionObtained){
          automatonDefinitionObtained = true;
          if(this.isJsonValid(data[0])){
            automatonDefinition = data[0];
            this.render();
          }
        }else{
          currentState = Math.floor(Math.random() * 3)
          //currentState = data[0].value;
          this.render();
        }
      }

      render() {
        if (automatonDefinition) {
          automatonDrawer = new AutomatonGraphDrawer();
          automatonDrawer.drawGraph(automatonDefinition, cyContainer, currentState, this.panel.transitionsColor);
        }
      }

    }

    AutomatonVisualizer_Ctrl.templateUrl = 'module.html';

    ```
  
    If you're using a metrics plugin, you need to bind the event **"data-received"** in the constructor to listen to this event.
    
    So after you've attached the datasource to your custom plugin, every time Grafana refresh the dashboard a query is made and the response data is passed in this event. 

  * **module.html** is simply the html shown in the panel container. It can be styled with stylesheets inside the css folder.

  * **editor.html** is the html show in the additional configuration tabs you've inserted in the plugin. 
  
  * Other utility js files are allowed in the src folder.



  

