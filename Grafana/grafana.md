# Grafana

Grafana is a dashboard creator for showing graphs and a variety of widget relatively to the report of time-series data.

Grafana permit to attach to it several dataSource like:

* [Graphite](https://graphiteapp.org/)
* [Prometeous](https://prometheus.io/)
* [Elastic search](https://www.elastic.co/)
etc...

